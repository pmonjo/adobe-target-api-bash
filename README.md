# Adobe Target API Bash

A Bash script to make calls to the Adobe Target API (https://developers.adobetarget.com/api/)

# Random notes

 - Generates a file `bearer.json` for caching purposes
 - Requires following applications installed: curl, jq and openssl

# Commands that are currently supported

 - activities
 - environments
 - mboxes
 - properties