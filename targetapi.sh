#!/bin/bash

set -euf -o pipefail

# Cache of bearer token
bearer_json="bearer.json"

# Get the command name
me="$0"

# Process command line arguments
config=""
while getopts "rc:h" opt; do
	case "$opt" in
		c )
			config=$OPTARG
			;;
		h )
			echo "Usage:"
			echo "    $me -h                      Display this help message."
			echo "    $me -c config <command>     Run with the given configuration file."
			exit 0
			;;
		\? )
			exit 1
			;;
		: )
			exit 1
			;;
	esac
done
shift $((OPTIND -1))

### Check if jq, curl and openssl are there

# Read from configuration file
if [ -z "$config" ] ; then
	>&2 echo "$me: config file missing"
	exit 1
fi
if [ ! -r "$config" ] ; then
	>&2 echo "$me: unable to open config file: $config"
	exit 1
fi
conf_parsed=$(jq -r '. | to_entries | .[] | .key + "=\"" + .value + "\""' < "$config")
eval "$conf_parsed"

### Check that the parameters in the JSON configuration file are as expected

# Set the header and payload for JWT
header='{"alg": "RS256"}'
payload=$( jq -n \
	--arg org_id "$org_id" \
	--arg tech_account_id "$tech_account_id" \
	--arg aud "https://ims-na1.adobelogin.com/c/$api_key" \
	--argjson time $(( `date '+%s'` + 86400 )) \
	'{ 
		"iss": $org_id, 
		"sub": $tech_account_id, 
		"https://ims-na1.adobelogin.com/s/ent_marketing_sdk": true,
		"aud": $aud,
		"exp": $time
	}'
)

# Generate JWT (https://stackoverflow.com/questions/46657001/how-do-you-create-an-rs256-jwt-assertion-with-bash-shell-scripting)
b64enc() { openssl enc -base64 -A | tr '+/' '-_' | tr -d '='; }
json() { jq -c . | LC_CTYPE=C tr -d '\n'; }
rs_sign() { openssl dgst -binary -sha256 -sign "$private_key"; }
signed_content="$(json <<<"$header" | b64enc).$(json <<<"$payload" | b64enc)"
sig=$(printf %s "$signed_content" | rs_sign | b64enc)
jwt="${signed_content}"."${sig}"

# Check if bearer token has expired
expired=true
if [ -e "$bearer_json" ] ; then
	expires_in=$(jq -r .expires_in "$bearer_json")
	last_mod=$(stat -c %Y "$bearer_json")
	expires=$(( last_mod + (expires_in/1000) ))
	now=$(date "+%s")
	if [ "$now" -lt "$expires" ] ; then
		expired=false
	fi
fi

# Get a new bearer token if it has expired
if [ "$expired" = true ] ; then
	curl -sS -X POST \
		"https://ims-na1.adobelogin.com/ims/exchange/jwt/?client_id=$api_key&client_secret=$client_secret&jwt_token=$jwt" \
		-o "$bearer_json"
fi
access_token=$(jq -r '.access_token' bearer.json)

# Make a GET request to the API
# @param $1: url after /target/ (no leading /)
# @param $2: accepted content type
function request_get {
	url="https://mc.adobe.io/$tenant/target/$1"
	curl -sS -X GET "$url" \
	  -H "Authorization: Bearer $access_token" \
	  -H 'Cache-Control: no-cache' \
	  -H "Accept: $2" \
	  -H "X-Api-Key: $api_key" \
	  | jq .
}

# Make a GET request to the API, expecting a v1 JSON response
# @param $1: url after /target/ (no leading /)
function call_api_v1 {
	request_get "$1" "application/vnd.adobe.target.v1+json"
}

# Make a GET request to the API, expecting a v2 JSON response
# @param $1: url after /target/ (no leading /)
function call_api_v2 {
	request_get "$1" "application/vnd.adobe.target.v2+json"
}

# Make a GET request to the API, expecting a v3 JSON response
# @param $1: url after /target/ (no leading /)
function call_api_v3 {
	request_get "$1" "application/vnd.adobe.target.v3+json"
}

# Main switch
if [ $# -eq 0 ] ; then
	>&2 echo "$me: missing command"
	exit 1
fi
case $1 in
	activities)
		call_api_v3 "activities"
		;;
	activity_ab)
		if [ $# -eq 1 ] ; then
			>&2 echo "$me: missing activity ID"
			exit 1
		fi
		call_api_v3 "activities/ab/$2"
		;;
	activity_xt)
		if [ $# -eq 1 ] ; then
			>&2 echo "$me: missing activity ID"
			exit 1
		fi
		call_api_v3 "activities/xt/$2"
		;;
	environments)
		call_api_v1 "environments"
		;;
	mboxes)
		call_api_v1 "mboxes"
		;;
	offers)
		call_api_v2 "offers"
		;;
	properties)
		call_api_v1 "properties"
		;;
	*)
		>&2 echo "$me: unrecognized command: $1"
		exit 1
esac
